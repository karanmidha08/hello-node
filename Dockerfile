FROM node:10.15-alpine AS builder
WORKDIR /app
COPY . /app
EXPOSE 3000
CMD [ "npm", "start" ] 

